<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Repository\ArtistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ArtistRepository $artistRepository)
    {
        /**
         * findAll() tout récupérer
         * findBy() récupérer par criteres
         * findOneBy() récupérer l entinté par critères
         * find() récupérer l entinté par ID
         */
        $resultats = $artistRepository->find(93);

        dd($resultats);

        return $this->render('home.html.twig');
    }
}
