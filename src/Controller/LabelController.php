<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/label", name="label_")
 */
class LabelController extends AbstractController
{
    /**
     * URI: /label/{id}
     * Nom: label_page
     * @Route("/{id}", name="page")
     */
    public function index($id)
    {
        return $this->json([
            'message' => 'Label',
            'path' => 'src/Controller/LabelController.php',
            'id' => $id,
        ]);
    }
}
