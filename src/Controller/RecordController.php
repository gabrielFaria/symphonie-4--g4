<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/record", name="record_")
 */
class RecordController extends AbstractController
{
    /**
     * URI: /record/{id}
     * Nom: record_page
     * @Route("/{id}", name="page")
     */
    public function index($id)
    {
        return $this->json([
            'message' => 'Record!',
            'path' => 'src/Controller/RecordController.php',
            'id' => $id
        ]);
    }
}
