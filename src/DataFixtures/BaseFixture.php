<?php


namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

abstract class BaseFixture extends Fixture
{
    /**@var ObjectManager*/
    private $manager;

    /**@var Generator*/
    protected $faker;

    /**
     * Méthode pour générer les entités
     * à implémenter par les classes qui héritent de BaseFixture
     */
    abstract protected function loadData(ObjectManager $manager);

    /**
     * Méthode appelée par le système des fixtures
     */
    public function load(ObjectManager $manager){
        // Enregistrement de l'ObjectManageur
        $this->manager = $manager;
        // Initialisation de Faker
        $this->faker = Factory::create('fr_FR');

        // Générer les donées
        $this->loadData($manager);
    }

    /**
     * Générer plusieurs entités
     *
     * @param int $count Nombre d'entités à générer
     * @param callable $factory Fonction qui crée et retourne l entité
     */
    protected function createMany(int $count, callable $factory){
        for ($i = 0; $i < $count; $i++) {
            // La factory doit retourner une entité
            $entity = $factory($i);

            // Vérifier qu'une entité ait été retournée
            if($entity === null) {
                throw new \LogicException('l entité doit être retournée');
            }

            // Préparation à l enrigistrement par Doctrine
            $this->manager->persist($entity);
        }
    }
}